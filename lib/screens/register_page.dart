import 'package:flutter/material.dart';
import 'package:smartren/components/bottom_nav_bar.dart';
import 'package:smartren/resources/color_resources.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();

}

class _RegisterPageState extends State<RegisterPage>{

  TextEditingController usernameController = TextEditingController();
  String fullName = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Colors.white,
            Colors.white,
          ],
        ),
      ),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Container(
            child: Column(
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Align(
                        alignment: FractionalOffset.topCenter,
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 15 ),
                          child: Column(
                            children: [
                              ShaderMask(
                                shaderCallback: (rect) {
                                  return LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [Colors.black, Colors.blueAccent],
                                  ).createShader(Rect.fromLTRB(0, 0, 900, 900));
                                },
                                blendMode: BlendMode.dstIn,
                                child: Container(
                                  padding: EdgeInsets.only(bottom: 20.0),
                                  height: 110 ,
                                  width: 120,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: AssetImage('assets/logo4x.png'),
                                      fit: BoxFit.cover,
                                    ), //DecorationImage
                                  ), //BoxDecoration
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 30.0),
                                child: Text("Create Account", style: TextStyle(color: ColorsResources.primary_color_alternative, fontWeight: FontWeight.w800, fontSize: 25),),
                              ),
                            ],
                          ),
                        ),
                      ),

                      Align(
                        alignment: FractionalOffset.topCenter,
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 10.0),
                          child:
                          Container(
                            margin: EdgeInsets.only (left: 90.0, right: 90.0),
                            child:
                            TextField(
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.supervised_user_circle_rounded),
                                border: OutlineInputBorder(),
                                labelText: 'Nama Lengkap',
                              ),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: FractionalOffset.topCenter,
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 10.0),
                          child:
                          Container(
                            margin: EdgeInsets.only (left: 90.0, right: 90.0),
                            child:
                            TextField(
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.menu_book_rounded),
                                border: OutlineInputBorder(),
                                labelText: 'NIS',
                              ),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: FractionalOffset.topCenter,
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 10.0),
                          child:
                          Container(
                            margin: EdgeInsets.only (left: 90.0, right: 90.0),
                            child:
                            TextField(
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.call),
                                border: new OutlineInputBorder(
                                  borderSide: new BorderSide(color: ColorsResources.primary_color),
                                ),
                                labelText: 'No. Telp',
                              ),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: FractionalOffset.topCenter,
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 10.0),
                          child:
                          Container(
                            margin: EdgeInsets.only (left: 90.0, right: 90.0),
                            child:
                            TextField(
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.mail),
                                border: new OutlineInputBorder(
                                  borderSide: new BorderSide(color: ColorsResources.primary_color),
                                ),
                                labelText: 'Alamat e-Mail',
                              ),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: FractionalOffset.topCenter,
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 30.0),
                          child:
                          Container(
                            margin: EdgeInsets.only (left: 90.0, right: 90.0),
                            child:
                            TextField(
                              obscureText: true,
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.lock),
                                border: new OutlineInputBorder(
                                  borderSide: new BorderSide(color: ColorsResources.primary_color),
                                ),
                                labelText: 'Password',
                              ),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: FractionalOffset.bottomCenter,
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 10.0),
                          child:
                          ElevatedButton(
                            child: Text('Sign Up', style: TextStyle(fontSize: 18)),
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context)=> BottomNavBar()));
                            },
                            style:
                            ElevatedButton.styleFrom(
                              primary: ColorsResources.primary_color_alternative,
                              fixedSize: Size(320, 50),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50)),
                            ),
                          ),
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Text("Sudah Punya Akun?", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.w300, fontSize: 18),),
                              ),
                              Container(
                                child: TextButton(
                                  child: Text("Daftar?", style: TextStyle(color: ColorsResources.primary_color_alternative, fontWeight: FontWeight.w300, fontSize: 18),),
                                  onPressed: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> BottomNavBar()));
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
      ),
    );

  }
}