import 'package:flutter/material.dart';
import 'package:smartren/resources/color_resources.dart';
import 'package:smartren/screens/user_page.dart';

class NotifikasiPage extends StatefulWidget {
  @override
  _NotifikasiPageState createState() => _NotifikasiPageState();

}

class _NotifikasiPageState extends State<NotifikasiPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                ColorsResources.primary_color,
                ColorsResources.secondary_color,
              ],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 42.0, left: 12.0, right: 0.0, bottom: 8.0),
                    child:
                    IconButton(
                      icon: Icon(Icons.arrow_back_rounded, color: Colors.white),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 43.0, left: 6.0, right: 0.0, bottom: 8.0),
                    child: Text('Notifikasi', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w800, fontSize: 22)),
                  ),
                  Flexible(fit: FlexFit.tight, child: SizedBox()),
                  Container(
                    padding: EdgeInsets.only(top: 42.0, left: 0.0, right: 20.0, bottom: 8.0),
                    child:
                    IconButton(
                      icon: CircleAvatar(
                        backgroundColor: Colors.white,
                      ),
                      iconSize: 24,
                        onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=> UserPage()));}
                    ),
                  ),
                ],
              ),

              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30),),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      spreadRadius: 1,
                      blurRadius: 5,
                      offset: Offset(0, -3),
                    ),
                  ],
                ),
                child: Column(
                  children: <Widget> [
                    Container(
                      padding: EdgeInsets.only(left: 8.0),
                      margin: const EdgeInsets.only(left: 28.0, right: 28.0, top: 25, bottom: 5),
                      decoration: BoxDecoration(
                        color: ColorsResources.primary_color800,
                      ),
                      child: Row(
                        children: <Widget> [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                padding: EdgeInsets.only(top: 8.0, left: 14.0, right: 0.0, bottom: 0.0),
                                child: Text('Libur Semester 1', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w600, fontSize: 14),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 8.0, left: 14.0, right: 0.0, bottom: 8.0),
                                child: Text('Hari, 29-09-2021', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                                ),
                              ),
                            ],
                          ),
                          Flexible(fit: FlexFit.tight, child: SizedBox()),
                          IconButton(
                            icon: Icon(
                              Icons.close,
                            ),
                            iconSize: 24,
                            onPressed: (){},
                          ),
                        ],
                      ),
                    ),
                    Divider(
                        indent: 38,
                        endIndent: 38,
                        color: Colors.black45
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 8.0),
                      margin: const EdgeInsets.only(left: 28.0, right: 28.0, top: 5, bottom: 5),
                      decoration: BoxDecoration(
                        color: ColorsResources.primary_color800,
                      ),
                      child: Row(
                        children: <Widget> [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                padding: EdgeInsets.only(top: 8.0, left: 14.0, right: 0.0, bottom: 0.0),
                                child: Text('Libur Semester 1', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w600, fontSize: 14),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(top: 8.0, left: 14.0, right: 0.0, bottom: 8.0),
                                child: Text('Hari, 29-09-2021', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                                ),
                              ),
                            ],
                          ),
                          Flexible(fit: FlexFit.tight, child: SizedBox()),
                          IconButton(
                            icon: Icon(
                              Icons.close,
                            ),
                            iconSize: 24,
                            onPressed: (){},
                          ),
                        ],
                      ),
                    ),
                    Divider(
                        indent: 38,
                        endIndent: 38,
                        color: Colors.black45
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
