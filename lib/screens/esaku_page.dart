import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:smartren/resources/color_resources.dart';
import 'package:smartren/screens/notifikasi_page.dart';
import 'package:smartren/screens/user_page.dart';

class EwalletPage extends StatefulWidget {
  @override
  _EwalletPageState createState() => _EwalletPageState();

}

class _EwalletPageState extends State<EwalletPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                ColorsResources.primary_color,
                ColorsResources.secondary_color,
              ],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 42.0, left: 12.0, right: 0.0, bottom: 0.0),
                    child:
                    IconButton(
                      icon: Icon(Icons.arrow_back_rounded, color: Colors.white),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 43.0, left: 6.0, right: 0.0, bottom: 0.0),
                    child: Text('e-Saku', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w800, fontSize: 22)),
                  ),
                  Flexible(fit: FlexFit.tight, child: SizedBox()),
                  Container(
                    padding: EdgeInsets.only(top:42.0, left: 0.0, right: 0.0, bottom: 0.0),
                    child:
                    IconButton(
                      icon: Image.asset("assets/bell.png",  fit: BoxFit.contain, width: 18,),
                      iconSize: 29,
                      onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=> NotifikasiPage()));},
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 42.0, left: 0.0, right: 20.0, bottom: 0.0),
                    child:
                    IconButton(
                      icon: CircleAvatar(
                        backgroundColor: Colors.white,
                      ),
                      iconSize: 24,
                        onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=> UserPage()));}
                    ),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 16.0, left: 36.0, right: 0.0, bottom: 16.0),
                    child: Text('Nomor Briva', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w800, fontSize: 20)),
                  )
                ],
              ),
              Column(
                children: <Widget> [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 16.0, left: 0.0, right: 0.0, bottom: 32.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12,
                              spreadRadius: 1,
                              blurRadius: 5,
                              offset: Offset(0, -3),
                            ),
                          ],
                        ),
                        child:
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              padding: EdgeInsets.only(top: 12.0, left: 21.0, right: 21.0, bottom: 0.0),
                              child: Text('Saldo e-Saku', style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w800, fontSize: 18)),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 6.0, left: 21.0, right: 21.0, bottom: 12.0),
                              child: Text('Rp. 1.000.000', style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w600, fontSize: 16)),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 16.0, left: 0.0, right: 0.0, bottom: 32.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12,
                              spreadRadius: 1,
                              blurRadius: 5,
                              offset: Offset(0, -3),
                            ),
                          ],
                        ),
                        child:
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              padding: EdgeInsets.only(top: 12.0, left: 21.0, right: 21.0, bottom: 0.0),
                              child: Text('Saldo Tabungan', style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w800, fontSize: 18)),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 6.0, left: 21.0, right: 21.0, bottom: 12.0),
                              child: Text('Rp. 100.420', style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w600, fontSize: 16)),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30),),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      spreadRadius: 1,
                      blurRadius: 5,
                      offset: Offset(0, -3),
                    ),
                  ],
                ),
                  child: Column(
                    children: <Widget> [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            padding: EdgeInsets.only(bottom: 30.0, top: 40),
                            child:
                            ElevatedButton(
                              child: Text('Paket', style: TextStyle(fontSize: 18)),
                              onPressed: (){},
                              style:
                              ElevatedButton.styleFrom(
                                primary: ColorsResources.primary_color_alternative,
                                fixedSize: Size(300, 60),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            padding: EdgeInsets.only(bottom: 30.0),
                            child:
                            ElevatedButton(
                              child: Text('Tagihan', style: TextStyle(fontSize: 18)),
                              onPressed: (){},
                              style:
                              ElevatedButton.styleFrom(
                                primary: ColorsResources.primary_color_alternative,
                                fixedSize: Size(300, 60),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            padding: EdgeInsets.only(bottom: 30.0),
                            child:
                            ElevatedButton(
                              child: Text('Riwayat Pembayaran', style: TextStyle(fontSize: 18)),
                              onPressed: (){},
                              style:
                              ElevatedButton.styleFrom(
                                primary: ColorsResources.primary_color_alternative,
                                fixedSize: Size(300, 60),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            padding: EdgeInsets.only(bottom: 30.0),
                            child:
                            ElevatedButton(
                              child: Text('Transaksi e-Saku', style: TextStyle(fontSize: 18)),
                              onPressed: (){},
                              style:
                              ElevatedButton.styleFrom(
                                primary: ColorsResources.primary_color_alternative,
                                fixedSize: Size(300, 60),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)),
                              ),
                            ),
                          ),
                        ],
                      ),
                      ],
                  ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}