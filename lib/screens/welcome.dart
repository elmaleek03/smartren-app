import 'package:flutter/material.dart';
import 'package:smartren/resources/color_resources.dart';
import 'package:smartren/screens/login_page.dart';
import 'package:smartren/screens/register_page.dart';

class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();

}

class _WelcomeState extends State<Welcome>{

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Colors.white,
              Colors.white,
            ],
          ),
        ),
    child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          child: Column(
            children: [
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Align(
                      alignment: FractionalOffset.topCenter,
                      child: Padding(
                        padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height / 6 ),
                        child: Column(
                          children: [
                            ShaderMask(
                              shaderCallback: (rect) {
                                return LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [Colors.black, Colors.blueAccent],
                                ).createShader(Rect.fromLTRB(0, 0, 900, 900));
                              },
                              blendMode: BlendMode.dstIn,
                              child: Container(
                                height: 110 ,
                                width: 120,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage('assets/logo4x.png'),
                                    fit: BoxFit.cover,
                                  ), //DecorationImage
                                ), //BoxDecoration
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(20),
                              child: Text("Selamat Datang!", style: TextStyle(color: ColorsResources.primary_color_alternative, fontWeight: FontWeight.w800, fontSize: 25),),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: FractionalOffset.bottomCenter,
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 30.0),
                        child:
                        ElevatedButton(
                          child: Text('Sign In', style: TextStyle(fontSize: 18)),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> LoginPage()));
                          },
                          style:
                          ElevatedButton.styleFrom(
                            primary: ColorsResources.primary_color_alternative,
                            fixedSize: Size(300, 50),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: FractionalOffset.bottomCenter,
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 80.0),
                        child:
                        ElevatedButton(
                          child: Text('Sign Up', style: TextStyle(fontSize: 18)),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> RegisterPage()));
                          },
                          style:
                          ElevatedButton.styleFrom(
                              primary: ColorsResources.primary_color_alternative,
                              fixedSize: Size(300, 50),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50)),
                        ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
    ),
    );

  }
}