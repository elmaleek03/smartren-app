import 'package:flutter/material.dart';
import 'package:smartren/screens/buku_page.dart';
import 'package:smartren/screens/elibrary_page.dart';
import 'package:smartren/resources/color_resources.dart';
import 'package:smartren/screens/guru_page.dart';
import 'package:smartren/screens/notifikasi_page.dart';
import 'package:smartren/screens/profile_page.dart';
import 'package:smartren/screens/user_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();

}

class _HomePageState extends State<HomePage>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                ColorsResources.primary_color,
                ColorsResources.secondary_color,
              ],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 42.0, left: 24.0, right: 0.0, bottom: 0.0),
                    child:
                    IconButton(
                      icon: Image.asset("assets/logo.png",  fit: BoxFit.contain),
                      onPressed: () {},
                    ),
                  ),
                  Flexible(fit: FlexFit.tight, child: SizedBox()),
                  Container(
                    padding: EdgeInsets.only(top:42.0, left: 0.0, right: 0.0, bottom: 0.0),
                    child:
                    IconButton(
                      icon: Image.asset("assets/bell.png",  fit: BoxFit.contain, width: 18,),
                      iconSize: 29,
                      onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=> NotifikasiPage()));},
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 42.0, left: 0.0, right: 20.0, bottom: 0.0),
                    child:
                    IconButton(
                      icon: CircleAvatar(
                        backgroundColor: Colors.white,
                      ),
                      iconSize: 24,
                        onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=> UserPage()));}
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 8.0, left: 30.0, right: 0.0, bottom: 14.0),
                    child: Text('Selamat Datang!', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w800, fontSize: 22)),
                  )
                ],
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30),),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      spreadRadius: 1,
                      blurRadius: 5,
                      offset: Offset(0, -3),
                    ),
                  ],
                ),
                child: Column(
                  children: <Widget> [
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 20.0, left: 34.0, right: 0.0, bottom: 16.0),
                          child: Text('Featured', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w600, fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget> [
                        Container(
                          child: Column(
                            children: <Widget> [
                              IconButton(
                                icon: Image.asset("assets/profil.png",  fit: BoxFit.contain),
                                iconSize: 46,
                                onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=> ProfilPage()));},
                              ),
                              Text('Profil', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w400, fontSize: 14)),
                            ],
                          ),
                        ),
                        Container(
                          child: Column(
                            children: <Widget> [
                              IconButton(
                                icon: Image.asset("assets/guru.png",  fit: BoxFit.contain),
                                iconSize: 46,
                                onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=> GuruPage()));},
                              ),
                              Text('Guru', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w400, fontSize: 14)),
                            ],
                          ),
                        ),
                        Container(
                          child: Column(
                            children: <Widget> [
                              IconButton(
                                icon: Image.asset("assets/buku.png",  fit: BoxFit.contain),
                                iconSize: 46,
                                onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=> BukuPage()));},
                              ),
                              Text('Buku', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w400, fontSize: 14)),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 0.0, left: 0.0, right: 0.0, bottom: 18.0),
                          child: Column(
                            children: <Widget> [
                              IconButton(
                                icon: Image.asset("assets/e-library.png",  fit: BoxFit.contain),
                                iconSize: 46,
                                onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=> ElibraryPage()));},
                              ),
                              Text('e-Library', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w400, fontSize: 14)),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 20.0, left: 34.0, right: 0.0, bottom: 16.0),
                          child: Text('Daftar Tugas Saya', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w600, fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget> [
                        Container(
                          padding: EdgeInsets.only(left: 8.0),
                          margin: const EdgeInsets.only(left: 28.0, right: 28.0, top: 5, bottom: 5),
                          decoration: BoxDecoration(
                            color: ColorsResources.primary_color800,
                          ),
                          child: Row(
                            children: <Widget> [
                              SizedBox(
                                width: 6.0,
                                height: 42.0,
                                child: const DecoratedBox(
                                  decoration: const BoxDecoration(
                                      color: Colors.red
                                  ),
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 8.0, left: 14.0, right: 0.0, bottom: 0.0),
                                    child: Text('Mata Pelajaran', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w600, fontSize: 14),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 8.0, left: 14.0, right: 0.0, bottom: 8.0),
                                    child: Text('Hari, 29-09-2021', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                              Flexible(fit: FlexFit.tight, child: SizedBox()),
                              IconButton(
                                icon: Icon(
                                  Icons.close,
                                ),
                                iconSize: 24,
                                onPressed: (){},
                              ),
                            ],
                          ),
                        ),
                        Divider(
                            indent: 38,
                            endIndent: 38,
                            color: Colors.black45
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 8.0),
                          margin: const EdgeInsets.only(left: 28.0, right: 28.0, top: 5, bottom: 5),
                          decoration: BoxDecoration(
                            color: ColorsResources.primary_color800,
                          ),
                          child: Row(
                            children: <Widget> [
                              SizedBox(
                                width: 6.0,
                                height: 42.0,
                                child: const DecoratedBox(
                                  decoration: const BoxDecoration(
                                      color: Colors.green
                                  ),
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 8.0, left: 14.0, right: 0.0, bottom: 0.0),
                                    child: Text('Mata Pelajaran', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w600, fontSize: 14),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 8.0, left: 14.0, right: 0.0, bottom: 8.0),
                                    child: Text('Hari, 29-09-2021', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                              Flexible(fit: FlexFit.tight, child: SizedBox()),
                              IconButton(
                                icon: Icon(
                                  Icons.close,
                                ),
                                iconSize: 24,
                                onPressed: (){},
                              ),
                            ],
                          ),
                        ),
                        Divider(
                            indent: 38,
                            endIndent: 38,
                            color: Colors.black45
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 8.0),
                          margin: const EdgeInsets.only(left: 28.0, right: 28.0, top: 5, bottom: 5),
                          decoration: BoxDecoration(
                            color: ColorsResources.primary_color800,
                          ),
                          child: Row(
                            children: <Widget> [
                              SizedBox(
                                width: 6.0,
                                height: 42.0,
                                child: const DecoratedBox(
                                  decoration: const BoxDecoration(
                                      color: Colors.yellow
                                  ),
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 8.0, left: 14.0, right: 0.0, bottom: 0.0),
                                    child: Text('Mata Pelajaran', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w600, fontSize: 14),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 8.0, left: 14.0, right: 0.0, bottom: 8.0),
                                    child: Text('Hari, 29-09-2021', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                              Flexible(fit: FlexFit.tight, child: SizedBox()),
                              IconButton(
                                icon: Icon(
                                  Icons.close,
                                ),
                                iconSize: 24,
                                onPressed: (){},
                              ),
                            ],
                          ),
                        ),
                        Divider(
                            indent: 34,
                            endIndent: 34,
                            color: Colors.black45
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 8.0),
                          margin: const EdgeInsets.only(left: 28.0, right: 28.0, top: 5, bottom: 5),
                          decoration: BoxDecoration(
                            color: ColorsResources.primary_color800,
                          ),
                          child: Row(
                            children: <Widget> [
                              SizedBox(
                                width: 6.0,
                                height: 42.0,
                                child: const DecoratedBox(
                                  decoration: const BoxDecoration(
                                      color: Colors.blue
                                  ),
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 8.0, left: 14.0, right: 0.0, bottom: 0.0),
                                    child: Text('Mata Pelajaran', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w600, fontSize: 14),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 8.0, left: 14.0, right: 0.0, bottom: 8.0),
                                    child: Text('Hari, 29-09-2021', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                              Flexible(fit: FlexFit.tight, child: SizedBox()),
                              IconButton(
                                icon: Icon(
                                  Icons.close,
                                ),
                                iconSize: 24,
                                onPressed: (){},
                              ),
                            ],
                          ),
                        ),
                        Divider(
                            indent: 34,
                            endIndent: 34,
                            color: Colors.black45
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
      ),
    ),
    );

  }
}