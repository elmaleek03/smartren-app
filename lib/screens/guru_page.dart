import 'package:flutter/material.dart';
import 'package:smartren/resources/color_resources.dart';
import 'package:smartren/screens/notifikasi_page.dart';

class GuruPage extends StatefulWidget {
  @override
  _GuruPageState createState() => _GuruPageState();

}


class _GuruPageState extends State<GuruPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                ColorsResources.primary_color,
                ColorsResources.secondary_color,
              ],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 42.0, left: 12.0, right: 0.0, bottom: 0.0),
                    child:
                    IconButton(
                      icon: Icon(Icons.arrow_back_rounded, color: Colors.white),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 43.0, left: 6.0, right: 0.0, bottom: 0.0),
                    child: Text('Guru', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w800, fontSize: 22)),
                  ),
                  Flexible(fit: FlexFit.tight, child: SizedBox()),
                  Container(
                    padding: EdgeInsets.only(top:42.0, left: 0.0, right: 0.0, bottom: 8.0),
                    child:
                    IconButton(
                      icon: Image.asset("assets/bell.png",  fit: BoxFit.contain, width: 18,),
                      iconSize: 29,
                      onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=> NotifikasiPage()));},
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 42.0, left: 0.0, right: 20.0, bottom: 8.0),
                    child:
                    IconButton(
                      icon: CircleAvatar(
                        backgroundColor: Colors.white,
                      ),
                      iconSize: 24,
                      onPressed: (){},
                    ),
                  ),
                ],
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30),),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      spreadRadius: 1,
                      blurRadius: 5,
                      offset: Offset(0, -3),
                    ),
                  ],
                ),
                child: Column(
                  children: <Widget> [
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 20.0, left: 34.0, right: 0.0, bottom: 16.0),
                          child: Text('Guru', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w600, fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget> [

                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
