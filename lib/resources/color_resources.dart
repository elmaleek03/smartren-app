import 'package:flutter/material.dart';

class ColorsResources {
  static const primary_color = Color(0xFF3FDDAD);
  static const primary_color800 = Color(0xFFF2FFFD);
  static const primary_color_alternative = Color(0xFF4FC09E);
  static const secondary_color = Color(0xFF01AFE5);
  static const primary_text_color = Color(0xFF464646);
}