import 'package:flutter/material.dart';
import 'package:smartren/screens/welcome.dart';

void main() {
  runApp(Smartren());
}

class Smartren extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Smartren',
      home: Welcome(),
      debugShowCheckedModeBanner: false,
    );
  }
}

